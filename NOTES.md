- Verifier comportement redis avec un volume read-only (cache in-memory?): https://hub.docker.com/_/redis/ https://stackoverflow.com/questions/27681402/how-to-disable-redis-rdb-and-aof https://redis.io/commands/readonly
- Clarifier quand boot.sh est dans app et qu'il se fait écraser par l'instruction volume du dockerfile
- Clarifier entrypoint et CMD
-  redis-server --appendonly yes ou REDIS_REPLICATION_MODE=slave ?
- Problèmes de place sur le disque de la VM par défaut (prendre image python3 plus légère ? fournir VM avec plusieurs disques ?)
- erreurs python lors du build de microblog v0.18 (partie facultative tp4) : alpine ne possède pas les deps requises, réadapter avec ptyhon:3. marche
- Utiliser un cache pour pip et refaire tp4 pour ne pas jouer sur les permissions. Ajouter un autre exemple sympa. Abandonner uwsgi ?
- Slides plus sexy : + d'illustrations et d'exemples pour la partie VM vs. container, lire plus dessus aussi
- CodeWave ? En tout cas faire ajout un par un dockerfile
- Des TD plus funs : funkwhale ? faire du docker machine et du basic swarm ? faire ressortir les intérêts du truc. Simuler une montée en charge en trouvant une manière de monitorer ?
- Pad CodiMD monté en Docker pour toute la classe ? Autres services ?

- demo de pb d'isolation entre deux process (version dependency) quand pas containers / ou entre process et OS pour introduire chroot, même si ; To be clear, this is NOT a vulnerability. The **root user is supposed to be able to change the root directory for the current process and for child processes**. Chroot only jails non-root processes. Wikipedia clearly summarises the limitations of chroot." Wikipédia : "On most systems, chroot contexts do not stack properly and chrooted programs with sufficient privileges may perform a second chroot to break out. To mitigate the risk of this security weakness, chrooted programs should relinquish root privileges as soon as practical after chrooting, or other mechanisms – such as FreeBSD jails – should be used instead. "
> En gros chroot fait que changer le root, si on peut rechroot on peut rechroot. Aussi, pb. d'isolation network et IPC. si privilégié pour le faire (du coup tempérer le "filesystem-based" d'Unix)
http://pentestmonkey.net/blog/chroot-breakout-perl

- expliquer chroot 
(quentin adam)
- jail est une upgrade sur chroot, pour FreeBSD, et concepts repris avec les linux namespaces (introduit 2002 et correctement mis en 2013).
- 6 namespaces:
    PID namespace provides isolation for the allocation of process identifiers (PIDs), lists of processes and their details. While the new namespace is isolated from other siblings, processes in its “parent” namespace still see all processes in child namespaces—albeit with different PID numbers.
    Network namespace isolates the network interface controllers (physical or virtual), iptables firewall rules, routing tables etc. Network namespaces can be connected with each other using the “veth” virtual Ethernet device.
    “UTS” namespace allows changing the hostname.
    Mount namespace allows creating a different file system layout, or making certain mount points read-only.
    IPC namespace isolates the System V inter-process communication between namespaces.
    User namespace isolates the user IDs between namespaces.

- Puis cgroups, àpartir de 2008 : une seule chose, limitation d'accès de ressources pour un processus (et ses enfants), pr limiter sa capacité à agir sur le hardware

Du coup déf d'un conteneur : **c'est un process avec un set de permissions**, pas une boîte, pas de la virtualisation.
Containers = Group of Linux Processes + Namespaces + cgroups

- mais y a des sides effects : 
  - docker fork bomb dans un container : limite des process lancées
    - si les autres process peuvent pas forker et qu'ils focntionennt avec ce mécanisme ils plantent
(parenthèse systemd : docker daemon et systemd en cocurrence pour être tous les deux des process d'init : pas possible de lancer un conteneur depuis systemd) (2e parenthèse : pid 1)

- systemd-nspawn : c'est quoi ?

  - VM oui c'est + une "boite noire" : même si exploit de l'hyperviseur possible
- pb clé de la virtualisation : faire croire à l'accès à un CPU. Et question de comment ça s'implémente : au niveau amtériel (VT-X qui permet le basculement du contexte des instructions mémoire super rapidement), ou au niveau uniquement logiciel ?
  - psaux sur hyperviseur : un seul process : kvm et des sous process kvm
  - performances VM peuvent être optimales : 6sec parfois si correctement optimisé
  - RAM est-elle un facteur limite ? non c'est pas cher
  - Les CPU pareil : on est rarement bloqués par puissance du CPU.
  - Le vrai problème c'est l'I/O : accès au storage et au network
    - en rélaité docker est pas si ouf quand on mesure la sécurité réseau : NATting et bridging lent
    - pareil pour l'OverlayFS qui sera improved par BtrFS : c'est la base de docker mais c'est super lent
    - alpine par exemple c'est uclibc donc un glibc recodé par un seul mec : y a des erreurs de compilation sur par exemple compilation d'une JVAPP java et on sait pas pourquoi : du coup l'argument de dire "c'est le même binaire de A à Z", à relativiser car alpine a pas du tout les mêmes binaires par exemplee t donc plus fragile

- comprendre l'isolation container : concrètement quand on fait tourner des containers de gens différents dans le même docker c'est "cmme de l'hébergement mutuel php sur la même machine avec apache qui segmente" (effectivement en interne ça l'est)


- ajouter l'explication de la commande "HEALTHCHECK"

- pet vs. cattle : du bétail par rapport à un chat qu'on amène au véto etc. Le conteneur amène cette logique
- le stateless c'est bien beau mais avec une bdd ça se gère pas magiquement du tout

(- approfondir à l'oral la logique de volume from ? : un conteneur vide, pointeur vers les volumes, du coup il suffit de faire volume from pr un nv conteneur)

- pb des volumes partagés / répliqués
  1. solution 1 : applicative
     -  réplication / sharding / clustering : difficiel à configurer
  2.  volume driver : infinit racheté par docker, flocker, convoy, visent à intégrer une techno de réplication : NFS/DRDB/EBS...
      -  c'est un moyen, pas une solution : le NFS est à configurer par nous


- faire un test de NFS ? ou autre test de volume partagé genre cluster Elasticsearch?

- question du monitoirng et d'un container UP mais zombie, ne fonctionne pas. Du coup logs. A centraliser pas à la main. Petit TP logs?
  - /var/lib/docker/containers/<id>/<id>-json.log
  pareil, solution 1: apllicatif, ELK
  solution 2: logging driver, le default c'est json-file, sinon ça peut etre syslog, gelf, fluentd.
  solution 3: des agents : des conteneurs auxquels on donne accès à la socket du host (docker.sock). Le fonctionenement par agent est très puissant. Ex de stack: cAdvisor/influxDB/Grafana.

- docker in docker on ne fait que lui donner accès à la socket du host : c'est en fait du "sidecar"


- Potasser : https://docs.docker.com/engine/security/security/


- différence en sécurité des VM c'est qu'on s'appuie pour les VM sur un sandboxing au niveau matériel (failles dans IOMMU/VT-X/instrctions x84) (si l'on oublie qu'un soft comme virtualbox a une surface d'attaque plus grade, par exemple exploit sur driver carte réseau) et dans l'autre faille de kernel

- Réviser réseau NAT de Docker 
- Pas donner accès au git juste à un site grav ou gatsby


- test rootless docker : https://docs.docker.com/engine/security/rootless/#prerequiresites





Docker logs monitoring :

* Dockbeat
* https://logz.io/blog/docker-logging/ : ELK avec Filebeat et docker collector, ou bien custom logging driver for docker avec syslog

+ https://docs.docker.com/swarm/plan-for-production/
+ Swarm TLS


Améliorer démo avec mDNS ? https://github.com/Jack12816/docker-mdns

La sécurité de Docker c'est aussi celle de la chaîne de dépendance des packages npm etc. : on fait confiance à trop de petites briques

https://github.com/veggiemonk/awesome-docker
https://github.com/jesseduffield/lazydocker