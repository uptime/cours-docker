---
title: Conteneurs Docker
class: animation-fade
layout: true
---

class: middle

# Conteneurs Docker
## *Modularisez et maîtrisez vos applications*

![](images/Docker-Logo-White-RGB_Horizontal.png)

---


# Bonjour !
## À propos de vous

- Quels sont vos besoins ?

---

## À propos de moi

> Hadrien Pélissier


Formateur en DevOps et sécurité informatique
Ex-ingénieur sécurité / DevOps

---

# Un peu de logistique

- Les slides de présentation et les TD sont disponibles à l'adresse https://hadrienpelissier.fr/docker
--

- Pour exporter les TD utilisez la fonction d'impression pdf de google chrome.

-- 

- Nous allons prendre des notes en commun sur un pad interactif CodiMD et par là faire une rapide démo de Docker.

---

# Introduction

.center[![](images/Moby-logo.png)]
---

# Des conteneurs


.col-6[.center[![](img/docker/docker.png)]]
.col-5[

- La métaphore docker : "box it, ship it"
--

- Une abstraction qui ouvre de nouvelles possibilités pour la manipulation logicielle.
- Permet de standardiser et de contrôler la livraison et le déploiement.

]

---

# Démo

1. Je cherche comment déployer mon logiciel "CodiMD" avec Docker ~~sur Google~~ [dans la documentation officielle de mon logiciel](https://hackmd.io/c/codimd-documentation/%2Fs%2Fcodimd-docker-deployment).
2. Je trouve le fichier "docker-compose.yml" qui permet de déployer plusieurs conteneurs et de les faire interagir ensemble (nous reviendrons dessus en détail au chapitre 4).
3. Je le télécharge et je le place dans mon dossier de travail. J'ouvre un terminal à cet emplacement.
4. *Ici, on devrait étudier le fichier pour l'adapter et, par exemple, changer les mots de passe par défaut dans la configuration.*
5. Je fais `docker-compose up` et j'attends. Le logiciel devrait être configuré et disponible à l'adresse `0.0.0.0:3000`.
6. Je vais chercher mon IP publique, vous pouvez désormais joindre ce pad pour toute la classe.

---
7. Mais... attendez, l'adresse de pad est incompréhensible !
8. Ce qui aiderait serait de pouvoir rediriger mon IP vers le pad de la classe.
9. Je ne vais pas installer tout un serveur nginx juste pour ça... si ?
10. Ecrivons la configuration nginx :
    ```nginx
    server {
        $pad_id = "";
        listen   80;
        server_name  0.0.0.0;
        location / {
                proxy_pass         http://127.0.0.1:3000/;
        }
        location /pad {
                proxy_pass         http://127.0.0.1:3000/$pad_id;
        }
    ```
11. Lançons un conteneur Docker nginx se basant sur ma configuration :
    <!-- `docker run -d -v /tmp/config-nginx:/etc/nginx/ --name nginx-pad-proxy` -->
    <!-- https://github.com/schmunk42/docker-nginx-redirect -->
12. Rien qu'en tapant mon IP (nous n'avons pas configuré le DNS) et en contactant `/pad`, on devrait accéder au super pad !
13. Posez-y vos questions, et annotez toutes les astuces et conseils qui vous ont aidé ou aideraient les autres ?

<!-- ---
# Encore de la démo ?
## Ok, lançons Wordpress puis faisons un cluster ELK avec logstash pour y envoyer les logs nginx. -->

---

# Retour sur les technologies de virtualisation


On compare souvent les conteneurs aux machines virtuelles. Mais ce sont de très grosses simplifications.
Une chose essentielle à retenir : **les conteneurs utilisent les mécanismes internes du *Kernel de l'OS Linux* tandis que les VM tentent de communiquer avec l'OS pour directement avoir accès au matériel de l'ordinateur.**

.col-8[.center[![](img/devops/windows-server-virtual-machines-vs-containers.png)]]
.col-4[
- **VM** : une abstraction complète pour simuler des machines
  - un processeur, mémoire, appels systèmes, réseau, virtuels (etc.)

- **conteneur** : un découpage dans linux pour séparer des ressources.
]

---

# Origine : LXC (LinuX Containers)

- Originellement Docker était basé sur le projet **lxc**.
--

- Les conteneurs sont un vieux concept qui se rapproche de `chroot`, présent dans les systèmes unix depuis longtemps : "comme tout est fichier, changer la racine c'est comme changer de système".
--

- `jail` introduit par FreeBSD pour compléter chroot en isolant les processus (pour des raisons de sécurité).
--

- En 2005 Google commence le développement des **cgroups** : une façon de tagger les demandes de processeur et les appels systèmes pour les grouper et les isoler.

- En 2008 démarre le projet LXC qui chercher à rassembler les **cgroups**, le **chroot** et les **namespaces**.
--

- **namespaces**: une façon de compartimenter la mémoire et le systeme de fichier

---

# Origine : LXC (LinuX Containers)

- En 2013: Docker commence à proposer une meilleure finition et une interface simple qui facilite l'utilisation des conteneurs **lxc**. Puis propose aussi son cloud **Docker Hub** pour faciliter la gestion des images.
--

- Au fur et à mesure Docker abandonne le code de **lxc** (mais continue d'utiliser les **cgroups** et **namespaces**).
--

- Le code de base de Docker (notamment **runC**) est open source : l'**Open Container Initiative** vise à standardiser et rendre robuste l'utilisation de containers.

---

# Bénéfices par rapport aux machines virtuelles

Docker permet de faire des "quasi machines" avec des performances proches du natif. 

- Vitesse d'exécution.
- Flexibilité sur les ressources (mémoire partagée).
- Moins **complexe** que la virtualisation
- Plus **standard** que les multiples hyperviseurs
- notamment moins de bugs d'interaction entre l'hyperviseur et le noyau

---

# Bénéfices par rapport aux machines virtuelles
VM et conteneurs proposent une flexibilité de manipulation des ressources de calcul mais les machines virtuelles sont trop lourdes pour être multipliées librement :
- elles ne sont pas efficaces pour isoler **chaque application**
- elles ne permettent pas la transformation profonde que permettent les conteneurs :
  - le passage à une architecture **microservices**
  - et donc la **scalabilité** pour les besoins des services cloud

---

# Pourquoi utiliser Docker ?

Docker est pensé dès le départ pour faire des conteneurs applicatifs :

- **Isoler** les modules applicatifs.
--

- Gérer les **dépendances** en les embarquant dans le conteneur.
--

- **Immutabilité** : la configuration d'un conteneur n'est pas faite pour être modifiée après sa création.
--

- **Cycle de vie court** -> pas de persistance (logique DevOps).

---

# Pourquoi utiliser Docker ?

Docker modifie beaucoup la **"logistique"** applicative.

- **uniformisation** face aux divers langages de programmation, configurations et briques logicielles
--

- **Installation sans accroc** et **automatisation** beaucoup plus facile.
--

- Permet de simplifier l'**intégration continue**, la **livraison continue** et le **déploiement continu**.
--

- **Rapproche les développeur·euses** des **opérations** (tout le monde utilise la même technologie).

--

- Permet l'adoption plus large de la logique DevOps (notamment le concept *d'infrastructure as code*)

---

# Positionnement sur le marché

- Docker est la technologie ultra-dominante sur le marché de la conteneurisation
  - La simplicité d'usage et le travail de standardisation (OCI) lui garantissent légitimité et fiabilité
  - La métaphore du conteneur fonctionne et la bonne documentation aide !
--

- **LXC** existe toujours et est devenu très agréable à utiliser, notamment avec **LXD** (développé par Canonical, l'entreprise derrière Ubuntu).
- Il a cependant un positionnement différent : faire des conteneurs pour faire tourner des OS linux complets.
--

- **RKT** : un autre container engine développé par **container linux** avec une architecture un peu différente. permet de faire tourner des images docker
--

- **Apache Mesos** : un logiciel de gestion de cluster qui permet de se passer de docker, mais propose quand même un support pour Docker et rkt depuis 2016.

---

# Terminologie et concepts fondamentaux

Ne pas confondre :

- Une **image** : un modèle pour créer un conteneur.
- Un **conteneur** : l'instance qui tourne sur la machine.
- Un **volume** : un espace virtuel pour gérer le stockage d'un conteneur et le partage entre conteneurs.

Autres concepts centraux:

- un **registry** : un serveur ou stocker des artefacts docker c'est à dire des images versionnées.
- un **orchestrateur** : un outil qui gère automatiquement le cycle de vie des conteneurs (création/suppression).

---

# Visualiser l'architecture Docker

## Daemon - Client - images - registry

![](img/docker/archi1.png)


---

# L'écosystème Docker

- **Docker Compose** : Un outil pour décrire des applications multiconteneurs.
--

- **Docker Machine** : Un outil pour gérer le déploiement Docker sur plusieurs machines depuis un hôte.
--

- **Docker Hub** : Le service d'hébergement d'images proposé par Docker Inc. (le registry officiel)
--

<!-- - (Docker **Cloud** et Docker **Store** ont fusionné avec **Docker Hub**) -->

---

# Docker **Community** ou **Enterprise**

Choisir une édition :

- **Community Edition** (Docker CE)
  - Version de référence la plus répandue.
  - Solide et déployée en production.
  - Peu de limitations, suffit pour quelqu'usage que ce soit.
  - Open source => potentielle vérification et corrections de bugs par la communauté.

---

- **Enterprise Edition** (Docker EE)
  - Version basée sur la version community
  - Version entreprise du Docker Engine
    - Une stack complète de plugins avec des fonctionnalités spécifiques de :
      - sécurité : principal argument mis en avant (contrôle des images, communications chiffrées, réseau sécurisé Kubernetes) ;
      - un panel d'outils pour une intégration plus facile de la CI/CD ;
      - du support technique.
  <!-- - Intéressant pour avoir une pile bien intégrée, sécurisée pour des entreprises qui veulent leur plateforme de conteneurs "on premise". -->

---

# Premier TD : on installe Docker et on joue avec
Commandes utiles :

Mentalité :
![](images/changingThings.jpg)

---