---
title: 'Docker 4 - Créer une application multiconteneur'
visible: true
---

## Démarrons une nouvelle application Flask 

- Démarrez un nouveau projet dans VSCode (créez un dossier appelé `identidock` et chargez-le avec la fonction *Add folder to workspace*)
- Dans un sous-dossier `app`, ajoutez une petite application python en créant ce fichier `identidock.py` :
  
```python
from flask import Flask, Response, request
import requests
import hashlib
import redis

app = Flask(__name__)
cache = redis.StrictRedis(host='redis', port=6379, db=0)
salt = "UNIQUE_SALT"
default_name = 'Joe Bloggs'

@app.route('/', methods=['GET', 'POST'])
def mainpage():

    name = default_name
    if request.method == 'POST':
        name = request.form['name']

    salted_name = salt + name
    name_hash = hashlib.sha256(salted_name.encode()).hexdigest()
    header = '<html><head><title>Identidock</title></head><body>'
    body = '''<form method="POST">
                Hello <input type="text" name="name" value="{0}">
                <input type="submit" value="submit">
                </form>
                <p>You look like a:
                <img src="/monster/{1}"/>
            '''.format(name, name_hash)
    footer = '</body></html>'
    return header + body + footer


@app.route('/monster/<name>')
def get_identicon(name):

    image = cache.get(name)

    if image is None:
        print ("Cache miss", flush=True)
        r = requests.get('http://dnmonster:8080/monster/' + name + '?size=80')
        image = r.content
    cache.set(name, image)

    return Response(image, mimetype='image/png')

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0', port=9090)

```



- `uWSGI` est un serveur python de production très adapté pour servir notre serveur intégré Flask, nous allons l'utiliser.


- Dockerisons maintenant cette nouvelle application avec le Dockerfile suivant :

```Dockerfile
FROM python:3.7

#Ajouter tout le contexte
ADD . .
RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi
RUN pip install Flask uWSGI requests redis
WORKDIR /app
COPY app/identidock.py /app

EXPOSE 9090 9191 
USER uwsgi
CMD ["uwsgi", "--http", "0.0.0.0:9090", "--wsgi-file", "/app/identidock.py", \
"--callable", "app", "--stats", "0.0.0.0:9191"]
```

- Observons le code du Dockerfile ensemble s'il n'est pas clair pour vous. Juste avant de lancer l'application, nous avons changé d'utilisateur avec l'instruction `USER`, pourquoi ?.

- Construire l'application, pour l'instant avec `docker build`, la lancer et vérifier avec `docker exec`, `whoami` et `id` l'utilisateur avec lequel tourne le conteneur.

## Faire varier la configuration en fonction de l'environnement

Le serveur de développement Flask est bien pratique pour debugger en situation de développement, mais n'est pas adapté à la production.
Nous pourrions créer deux images pour les deux situations mais ce serait aller contre l'impératif DevOps de rapprochement du dev et de la prod.

- Créons à la racine du projet (pas dans `app`) un script bash `boot.sh` pour adapter le lancement de l'application au contexte :

```bash
#!/bin/bash
set -e
if [ "$APP_ENVIRONMENT" = 'DEV' ]; then
    echo "Running Development Server"
    exec python "/app/identidock.py"
else
    echo "Running Production Server"
    exec uwsgi --http 0.0.0.0:9090 --wsgi-file /app/identidock.py --callable app --stats 0.0.0.0:9191
fi
```

- Déclarez maintenant dans le Dockerfile la variable d'environnement `APP_ENVIRONMENT` avec comme valeur par défaut `PROD`.

- Ajoutez une instruction pour copier le script de boot, le rendre exécutable, puis remplacez l'instruction `CMD` pour lancer le script de boot à la place.


## Articuler deux images avec Docker compose

- Vérifiez que docker-compose est bien installé avec `sudo apt-get install docker-compose`.
- A la racine de notre projet (à côté du Dockerfile), créez un fichier déclaration de notre application `docker-compose.yml` avec à l'intérieur:
  
```yml
version: '2'
services:
  identidock:
    build: .
    ports:
      - "9090:9090"
    environment:
      APP_ENVIRONMENT: DEV
    volumes:
      - ./app:/app
```

- Plusieurs remarques :
  - la première ligne déclare le conteneur de notre application
  - les lignes suivantes permettent de décrire comment lancer notre conteneur
  - `build: .` d'abord l'image d'origine de notre conteneur est le résultat de la construction du répertoire courant
  - la ligne suivante décrit le mapping de ports.
  - on définit ensuite la valeur de l'environnement de lancement du conteneur
  - on définit un volume (le dossier `app` dans le conteneur sera le contenu de notre dossier de code)

  - n'hésitez pas à passer du temps à explorer les options et commandes de `docker-compose`. Ainsi que [la documentation du langage (DSL) des Compose files](https://docs.docker.com/compose/compose-file/).

- Lancez le service (pour le moment mono-conteneur) avec `docker-compose up`
- Visitez la page web.
- Essayez de modifier l'application et de recharger la page web. Voilà comment, grâce à un volume on peut développer sans reconstruire l'image à chaque fois !

- Ajoutons maintenant un deuxième conteneur. Nous allons tirer parti d'une image déjà créée qui permet de récupérer une "identicon". Ajoutez à la suite du fichier Compose ***(attention aux indentations !)*** :

```yml
    links:
      - dnmonster

  dnmonster:
    image: amouat/dnmonster:1.0
```

Cette fois plutôt de construire l'image, nous indiquons simplement comment la récupérer sur le Docker Hub. Nous ajoutons également un lien qui indique à Docker de configurer le réseau convenablement.


- Ajoutons également un conteneur redis, la base de données qui sert à mettre en cache les images et à ne pas les recalculer à chaque fois :

```yml
  redis:
    image: redis:3.0
```

- Et un deuxième lien `- redis`  ***(attention aux indentations !)***.

- Créez un deuxième fichier compose `docker-compose.prod.yml` pour lancer l'application en configuration de production.

- Vérifiez dans les logs de l'application quand l'image a été générée et quand elle est bien mise en cache dans redis.

- N'hésitez pas à passer du temps à explorer les options et commandes de `docker-compose`. Ainsi que [la documentation du langage (DSL) des Compose files](https://docs.docker.com/compose/compose-file/).


## Assembler l'application Flask avec Compose

*Facultatif : si vous êtes en avance*


- Installez Elasticsearch via docker-compose en s'aidant de la [documentation officielle pour Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html).

- Vous pouvez ajouter un [Kibana](https://www.elastic.co/guide/en/kibana/current/docker.html) (interface web de Elasticsearch et bien plus) pour découvrir cet outil de recherche.
  - les paramètres par défaut suffisent pour Kibana en mettant simplement le conteneur dans un réseau commun. Vous pouvez donc supprimer les variables d'environnement de configuration.

- Enfin, créez un fichier Docker Compose pour faire fonctionner [l'application Flask finale du TP précédent](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers)  (à partir du tag git `v0.18`) avec MySQL et Elasticsearch.