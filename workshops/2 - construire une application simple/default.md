---
title: 'Docker 2 - Construire une application simple'
visible: true
---


## I – Découverte d'une application web flask

- Récupérez d’abord une application Flask exemple en la clonant sur le Bureau :
```bash
git clone https://github.com/miguelgrinberg/microblog/
```
- Déplacez-vous dans le dossier `microblog`

- Nous allons activer une version simple de l’application grâce à git : `git checkout v0.2`
   
- Ouvrez le dossier microblog cloné avec VSCode (Open Folder). Dans VSCode, vous pouvez faire Terminal > New Terminal pour obtenir un terminal en bas de l'écran.
   
<!-- - Pour la tester d’abord en local (sans conteneur) nous avons besoin des outils python. Vérifions s'ils sont installés :
    `sudo apt install python-pip python-dev build-essential` -->

<!-- - Créons l’environnement virtuel : `virtualenv -p python3 venv`

- Activons l’environnement : `source venv/bin/activate` -->

<!-- - Installons la librairie `flask` et exportons une variable d’environnement pour déclarer l’application.
    a) `pip install flask`
    b) `export FLASK_APP=microblog.py` -->

<!-- - Maintenant nous pouvons tester l’application en local avec la commande : `flask run` -->

<!-- - Visitez l’application dans le navigateur à l’adresse indiquée. -->
    
- Observons le code dans VSCode. Qu’est ce qu’un fichier de template ? Où se trouvent les fichiers de templates dans ce projet ?
 
- Changez le prénom Miguel par le vôtre dans l’application.
<!-- - Relancez l'app flask et testez la modification en rechargeant la page. -->

## II – Passons à Docker

Déployer une application Flask manuellement à chaque fois est relativement pénible. Pour que les dépendances de deux projets Python ne se perturbent pas, il faut normalement utiliser un environnement virtuel `virtualenv` pour séparer ces deux apps.

Nous allons donc construire une image de conteneur pour empaqueter l’application et la manipuler plus facilement. Assurez-vous que Docker est installé.

- Dans le dossier du projet ajoutez un fichier nommé `Dockerfile`

-  Ajoutez en haut du fichier : `FROM ubuntu:latest` Cette commande indique que notre conteneur de base est le conteneur officiel Ubuntu.
   
- Nous pouvons déjà contruire un conteneur à partir de ce modèle Ubuntu vide :
   `docker build -t microblog .`

- Une fois la construction terminée lancez le conteneur.
   
-  Le conteneur s’arrête immédiatement. En effet il ne contient aucune commande bloquante et nous n'avons précisé aucune commande au lancement. Pour pouvoir observer le conteneur convenablement il fautdrait faire tourner quelque chose à l’intérieur. Ajoutez à la  fin du fichier la ligne :
   `CMD ["/bin/sleep", "3600"]`
   Cette ligne indique au conteneur d’attendre pendant 3600 secondes comme au TP précédent.

- Reconstruisez le conteneur et relancez-le

- Affichez la liste des conteneurs en train de fonctionner

- Nous allons maintenant rentrer dans le conteneur en ligne de commande pour observer. Utilisez la commande : `docker exec -it <id_du_conteneur> /bin/bash`

- Vous êtes maintenant dans le conteneur avec une invite de commande. Utilisez quelques commandes Linux pour le visiter rapidement (`ls`, `cd`...).

- Il s’agit d’un Linux standard, mais il n’est pas conçu pour être utilisé comme un système complet, juste pour isoler une application. Il faut maintenant ajouter notre application flask à l’intérieur. Dans le Dockerfile supprimez la ligne CMD, puis ajoutez :
    - `RUN apt-get update -y`
    - `RUN apt-get install -y python-pip python-dev build-essential`

- Reconstruisez votre conteneur. Si tout se passe bien, poursuivez.

- Pour installer les dépendances python et configurer la variable d'environnement Flask ajoutez:

    - `RUN pip install flask`
    - `ENV FLASK_APP microblog.py`


- Ensuite, copions le code de l’application à l’intérieur du conteneur. Pour cela ajoutez les lignes :
```Dockerfile
COPY . /app
WORKDIR /app
```

Ces deux lignes indiquent de copier tout le contenu du dossier courant sur l'hôte dans un dossier /app à l’intérieur du conteneur. Puis le dossier courant dans le conteneur est déplacé à `/app`.

- Ensuite, pour démarrer l’application nous aurons besoin d’un script de boot. Créez un fichier `boot.sh` dans `app` avec à l’intérieur :

```bash
#!/bin/sh
flask run -h 0.0.0.0
```

- Enfin, ajoutons la section de démarrage à la fin du Dockerfile :
  
```Dockerfile
RUN chmod +x boot.sh
ENTRYPOINT ["./boot.sh"]
```

- Reconstruisez le conteneur et lancez-le en ouvrant le port `5000` avec la commande : `docker run -d -p 5000:5000 microblog`

- Naviguez dans le navigateur à l’adresse `localhost:5000` pour admirer le prototype microblog.

- Lancez un deuxième container cette fois avec : `docker run -d -p 5001:5000 microblog`

- Une deuxième instance de l’app est maintenant en fonctionnement et accessible à l’adresse localhost:5001

## Observons et optimisons l'image

<!-- - Changez le contenu du fichier `requirements.txt` puis relancez le build. -->

- Changez le contenu d'un des fichier python de l'application et relancez le build.

- Observez comme le build recommence à partir de l'instruction modifiée. Les layers précédents sont mis en cache par le Docker Engine

- Pour optimiser, rassemblez les commandes `RUN` liées à apt-get en une seule commande avec `&&` et sur plusieurs lignes avec `\`.

- Re-testez votre image.

<!-- - Finalement, avons nous besoin d'un **virtualenv** à l'intérieur d'un **conteneur Docker** ? C'est redondant : le conteneur isole déjà les dépendances d'une seule application.
  - modifier le Dockerfile pour installer les dépendances directement dans l'OS du conteneur (sur une seule ligne). -->

- Ajoutez après le `FROM` du `Dockerfile` une commande `LABEL maintainer=<votre_nom>`

- Ajoutez une commande `LABEL version=<votre_version>`

<!-- - Le shell par défaut de Docker est `SHELL ["/bin/sh", "-c"]`. Cependant ce shell a certains comportement inhabituels et la commande de construction **n'échoue pas forcément** si une commande s'est mal passée. Dans une optique d'intégration continue, pour rendre la modification ultérieure de l'image plus sûre ajoutez au début (en dessous des `LABEL`) `SHELL ["/bin/bash", "-eux", "-o", "pipefail", "-c"]`.

- Pour autant, le shell bash est non standard sur docker. Pour ne pas perturber les utilisateurs de l'image qui voudraient lancer des commande il peut être intéressant de rebasculer sur `sh` à la fin de la construction. Ajoutez à l'avant dernière ligne: `SHELL ["/bin/sh", "-c"]` -->
  
- Visitons **en root** (`sudo su`) le dossier `/var/lib/docker/` sur l'hôte. En particulier, `image/overlay2/layerdb/sha256/` :
  - On y trouve une sorte de base de données de tous les layers d'images avec leurs ancêtres.
  - Il s'agit d'une arborescence.

<!-- - Pour explorer la hiérarchie des images vous pouvez installer `https://github.com/wagoodman/dive` -->
- Trouvez la commande pour pousser l'image `microblog` sur le Docker Hub. Créez un compte le cas échéant.

- Affichez la liste des images présentes dans votre Docker Engine.

- Inspectez la dernière image que vous venez de créez.

- Observez l'historique de construction de l'image avec `docker image history <image>`

<!-- - Committez les modifications de votre dépôt avec `git` (faire le commit en local est suffisant). -->



- **Facultatif :** Vous pouvez aussi tentez de configurer votre propre registry dans un conteneur et poussez l'image dessus. 

<!-- ## Faire parler la vache
- Changez de répertoire et créez un nouveau Dockerfile qui permet de faire dire des choses à une vache grâce à la commande `cowsay`. Indice : utilisez la commande `ENTRYPOINT`.
Le but est de la faire fonctionner dans un conteneur à partir de commandes de type :
- `docker run cowsay Coucou !`
- `docker run cowsay Salut !`
- `docker run cowsay Bonjour !` -->

<!-- Faites que l'image soit la plus légère possible en utilisant l'image de base `alpine`. Attention, alpine possède des commandes légèrement différentes (`apk add` pour installer) et la plupart des programmes nes ont pas installés par défaut. -->

## La version plus complexe avec MySQL

*Facultatif : si vous êtes en avance*

- Basculez sur la version plus complexe : `git checkout v0.18`
- Suivez le [tutoriel de Miguel Grinberg](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers) pour packager cette version plus complexe avec un MySQL (à partir du paragraphe *Using Third-Party "Containerized" Services"*, mais en **s'arrêtant avant la partie finale sur Elasticsearch**), et adaptez/simplifiez si besoin.

<!-- ## Toujours plus loin : avec une base de données Elasticsearch

- **Facultatif :** suivez la fin du [tutoriel de Miguel Grinberg sur l'application Flask](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers) pour configurer Elasticsearch. -->